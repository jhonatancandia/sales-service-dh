package com.dh.fullstack.project.salesservice;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jhonatan Candia Romero
 */

//@EnableAutoConfiguration
@Configuration
@ComponentScan("com.dh.fullstack.project.salesservice")
@EnableFeignClients
public class Config {
}
