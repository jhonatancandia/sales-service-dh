package com.dh.fullstack.project.salesservice.model.domain;

/**
 * @author Jhonatan Candia Romero
 */
public enum Gender {
    MALE,
    FEMALE
}
