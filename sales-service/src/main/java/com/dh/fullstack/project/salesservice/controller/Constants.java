package com.dh.fullstack.project.salesservice.controller;

/**
 * @author Jhonatan Candia Romero
 */

public final class Constants {

    private Constants(){}

    public static class EmployeeTag{

        public static final String NAME = "Employee controller";

        public static final String DESCRIPTION = "employee operations";

        public static final String OPERATION_CREATE = "endpoint to create an employee";

        public static final String OPERATION_EMAIL = "endpoint to list an employee by email";

        public static final String OPERATION_GENDER = "endpoint to obtain employees of a certain gender";

        public static final String OPERATION_PAGINATION = "Endpoint to get employees using paging";

    }

    public static class BasePath{
        public static final String SYSTEM = "/system";

        public static final String SYSTEM_EMPLOYEE = SYSTEM + "/employees";

        public static final String API = "/api";

        public static final String API_EMPLOYEE = API + "/employees";

        public static final String SYSTEM_EMPLOYEE_GENDER = SYSTEM_EMPLOYEE + "/gender";

        public static final String API_EMPLOYEE_POSITION = API_EMPLOYEE + "/position";
    }
}
